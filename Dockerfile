FROM scratch
LABEL maintainer="JMiahMan <JMiahMan@unity-linux.org>"
ENV DISTTAG=mamabacontainer FGC=mamba FBR=mamba
ADD rootfs-i586.tar.xz /
CMD ["/bin/bash"]
